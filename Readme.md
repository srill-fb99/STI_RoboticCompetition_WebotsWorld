# Requirements

You need webots to run this simulation

# Compilation

You need to compile the different controllers.

You can do that either from webots directly or manually by launching make in 
the three subdirectories of the controllers directory

# Launching

Assuming you are in the root folder of the repository. 
You can launch the simulation with

```

webots worlds/sti_competition.wbt

```


